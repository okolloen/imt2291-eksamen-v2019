# imt2291 eksamen v2019
Lag en FORK av dette repositoriet og gi med leserettigheter (oeivind.kolloen@ntnu.no), det som ligger i repositoriet ved eksamenstidens slutt er regnet som din besvarelse.

Legg inn studentnummeret ditt øverst i denne filen (readme.md).

For oppgave 1 så skriver du inn svaret direkte under oppgaveteksten, for de andre oppgavene så angir dere under oppgaveteksten hvilke filer som inneholder koden for den enkelte oppgaven.

## Oppgave 1

Beskriv hva som skjer når du kjører "codecept run" i test containeren på docker oppsettet i katalogen oppgave1. Hva kjøres i hvilke containere, hvilke nettverkskall gjøres fra hvilke containere til hvilke containere og i hvilken rekkefølge.

### Oppgave 2-4

For disse oppgavene kan dere bruke oppsettet i katalogen *oppgave2-4* eller dere kan bruke standard php/mysql server (f.eks. XAMPP) men opprett da databasen fra *oppgave2-4/dbInit/myDB.sql*.

Kjør *composer install* i *oppgave2-4/www* katalogen for å installere twig.

NB, IKKE kjør "npm install" i *www* katalogen i *oppgave2-4*, se siste kommentaren i oppgave 4.

## Oppgave 2 (PHP)

Lag en side som presenterer en form for å lage en ny bruker (uten avatar bilde.) Dvs, brukeren skal skrive inn brukernavn (bruk e-post adresse som brukernavn), passord, fornavn og etternavn. Sjekk for gyldige data på både klient og server (dvs, gyldig e-post adresse, minimum lengde på passord, fornavn og etternavn.) La samme side (skript) ta i mot data som den siden som presenterer formen, lagre ny bruker i databasen og gi fornuftige tilbakemeldinger ved suksess/feil.
Bruk twig for å presentere data.

## Oppgave 3 (JavaScript)

NB, det finnes ferdige PHP skript i *oppgave2-4/www/api* for å hente/oppdatere informasjon.

Lag en side som lister ut alle brukere (med avatar bilde dersom det finnes, avatar bildet hentes fra *api/avatar.php*), hent informasjon om brukerne med fetch (i JavaScript, hentes fra *api/fetchUsers.php*). Dvs at informasjon om brukerne ikke finnes på siden når den siden hentes fra serveren. Presenter listen på venstre del av skjermen. Når en velger en bruker fra listen skal en kunne redigere informasjon om brukeren på høyre side av skjermen.

All informasjon om brukeren skal kunne redigeres med unntak av autogenerert ID i databasen. Send informasjon om brukernavn/fornavn/etternavn/passord til serveren når brukeren trykker en knapp (bruk fetch med metoden POST, sendes til *api/updateUser.php*). NB, for å oppdatere fornavn/etternavn trenger en ikke oppgi gammelt passord, men for å oppdatere brukernavn og eller passord så må det gamle passordet oppgis. Se *api/updateUser.php* for detaljer.

Det skal også være mulig å laste opp avatar bilde av brukeren. Opplasting av bilder *skal* gjøres med en progressbar (start opplastingen så snart brukeren har valgt et bilde, sendes til *api/updateAvatar.php*).

## Oppgave 4 (Web komponenter)

Lag en kopi av koden fra oppgave 3 (eller om du ikke fikk til oppgave 3, lag en enkel liste.) Du skal nå lage en web komponent (valgfritt om du bruker lit-element eller polymer-element) for å vise informasjonen for en og en bruker i listen til venstre. Dvs at innholdet i hvert enkelt element i listen skal være LI elementer som hver inneholder den nye taggen (web komponenten) som du lager. Du velger selv hvordan informasjon om brukeren overføres til komponenten. Beskriv de endringene du må gjøre i koden fra oppgave tre for at dette fortsatt skal fungere (hvordan finner du ID til den brukeren det trykkes på.)

NB, i *oppgave2-4/www/node_modules* ligger både lit-element og polymer-element med de endringer som skal til for at de fungerer uten å bruke "polymer serve" (dvs, koden fungerer direkte i docker/XAMPP).
